<?php

    namespace CmsTf\Validator\Tests;

    use CmsTf\Validator\Field\FieldDefinition;
    use CmsTf\Validator\Tests\Entity\User;
    use CmsTf\Validator\Validator;
    use PHPUnit\Framework\TestCase;

    /**
     * Class ValidatorTest
     *
     * @package CmsTf\Validator\Tests
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class ValidatorTest extends TestCase {
        /**
         * Test the translation.
         */
        public function testTranslation() {
            $validator = Validator::create(User::class, 'nl_NL');

            self::assertFalse($validator->validate(['username' => 'test', 'age' => - 5]));
            self::assertEquals('De minimale waarde is 0.', $validator->getError());
        }

        /**
         * Test merge.
         */
        public function testMerge() {
            $validator = Validator::create(['tags[*]' => ['count' => ['min' => 0]]]);
            self::assertCount(1, $validator->getDefinition('tags[*]')->getRules());
            $validator->add('tags[*]', ['count' => ['max' => 3]]);
            self::assertCount(1, $validator->getDefinition('tags[*]')->getRules());
            self::assertEquals(['min' => 0, 'max' => 3], $validator->getDefinition('tags[*]')
                ->getRule('count')
                ->getOptions());
        }

        /**
         * Test parse.
         */
        public function testParse() {
            $validator = Validator::create()
                ->add('rating', ['number' => ['min' => 0, 'max' => 5]])
                ->add('vat', ['number' => ['length' => 8]]);

            self::assertEquals([], $validator->parse([]));
            self::assertEquals(['rating' => null], $validator->parse(['rating' => '']));
            self::assertEquals(['vat' => 12345678, 'rating' => null], $validator->parse(['vat' => '12345678', 'rating' => '']));
            self::assertEquals(['rating' => 4], $validator->parse(['rating' => '4']));
        }

        /**
         * Test array.
         */
        public function testArray() {
            $validator = Validator::create([
                'username' => ['required'],
                'age' => ['number' => ['min' => 0]]
            ]);

            self::assertFalse($validator->validate([]));
            self::assertTrue($validator->validate(['username' => 'test']));
            self::assertFalse($validator->validate(['username' => 'test', 'age' => -5]));
            self::assertTrue($validator->validate(['username' => 'test', 'age' => 5]));
        }

        /**
         * Test annotations.
         */
        public function testAnnotation() {
            $validator = Validator::create(User::class);

            self::assertFalse($validator->validate([]));
            self::assertTrue($validator->validate(['username' => 'test']));
            self::assertFalse($validator->validate(['username' => 'test', 'age' => - 5]));
            self::assertTrue($validator->validate(['username' => 'test', 'age' => 5]));
            self::assertTrue($validator->validate(['username' => 'test', 'tags' => ['test']]));
            self::assertFalse($validator->validate(['username' => 'test', 'tags' => array_fill(0, 4, 'test')]));
        }

        /**
         * Test extra fields.
         */
        public function testExtraFields() {
            $validator = Validator::create()->add('foo');
            self::assertTrue($validator->validate([]));
            self::assertTrue($validator->validate(['foo' => 'bar']));
            self::assertFalse($validator->validate(['bar' => 'foo']));

            $validator->setAllowExtraFields(true);

            self::assertTrue($validator->validate([]));
            self::assertTrue($validator->validate(['foo' => 'bar']));
            self::assertTrue($validator->validate(['bar' => 'foo']));
        }

        /**
         * Test match.
         */
        public function testMatch() {
            // Test match.
            $validator = Validator::create()->add('foo[*]', ['number']);
            self::assertTrue($validator->validate(['foo[bar]' => '1']));
            self::assertFalse($validator->validate(['foo[bar]' => 'test', 'foo[foo]' => '1']));
            self::assertEquals(array('_form' => array(), 'foo[bar]' => array('This field has to be numeric.'), 'foo[foo]' => array()), $validator->getFieldErrors());
            self::assertEquals(array('foo[bar]' => array('This field has to be numeric.')), $validator->getFieldErrors(false));

            // Test FieldDefinition::isMatch
            $field = new FieldDefinition();
            $field->setName('test[*]');
            self::assertTrue($field->isMatch());
            $field->setName('test[\\*]');
            self::assertFalse($field->isMatch());
            $field->setName('test[\\*][*]');
            self::assertTrue($field->isMatch());
        }
    }

<?php
    namespace CmsTf\Validator\Tests\Rule;

    use CmsTf\Validator\Validator;
    use PHPUnit\Framework\TestCase;

    /**
     * Class EmailValidationTest
     *
     * @package CmsTf\Validator\Tests
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class EmailRuleTest extends TestCase {
        /**
         * Test the date validation.
         */
        public function testValidate() {
            $validator = Validator::create()->add('email', ['email']);

            self::assertTrue($validator->validate(['email' => 'test@test.nl']));
            self::assertFalse($validator->validate(['email' => 'invalid']));
            self::assertFalse($validator->validate(['email' => 'wait@what']));
        }
    }

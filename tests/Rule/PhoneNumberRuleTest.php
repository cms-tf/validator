<?php
    namespace CmsTf\Validator\Tests\Rule;

    use CmsTf\Validator\Validator;
    use PHPUnit\Framework\TestCase;

    /**
     * Class EmailValidationTest
     *
     * @package CmsTf\Validator\Tests
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class PhoneNumberRuleTest extends TestCase {
        /**
         * Test the date validation.
         */
        public function testValidate() {
            $validator = Validator::create()->add('phone_number', ['phone_number']);

            self::assertTrue($validator->validate(['phone_number' => '06-54672503']));
            self::assertFalse($validator->validate(['phone_number' => '06-546A2503']));
            self::assertFalse($validator->validate(['phone_number' => 'invalid']));
        }
    }

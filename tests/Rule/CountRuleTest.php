<?php
    namespace CmsTf\Validator\Tests\Rule;

    use CmsTf\Validator\Validator;
    use DateTime;
    use PHPUnit\Framework\TestCase;

    /**
     * Class CountValidationTest
     *
     * @package CmsTf\Validator\Tests
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class CountRuleTest extends TestCase {
        /**
         * Test the date validation.
         */
        public function testValidate() {
            $validator = Validator::create()->add('tags[*]', ['count' => ['min' => 1, 'max' => 3]]);

            self::assertFalse($validator->validate(['tags' => []]));
            self::assertTrue($validator->validate(['tags' => ['test']]));
            self::assertTrue($validator->validate(['tags' => array_fill(0, 3, 'test')]));
            self::assertFalse($validator->validate(['tags' => array_fill(0, 4, 'test')]));
        }
    }

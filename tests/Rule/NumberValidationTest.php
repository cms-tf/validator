<?php
    namespace CmsTf\Validator\Tests\Rule;

    use CmsTf\Validator\Validator;
    use PHPUnit\Framework\TestCase;

    /**
     * Class NumberValidationTest
     *
     * @package CmsTf\Validator\Tests
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class NumberValidationTest extends TestCase {
        /**
         * Test the number validation.
         */
        public function testValidate() {
            $validator = Validator::create()
                ->add('rating', ['number' => ['min' => 0, 'max' => 5]])
                ->add('vat', ['number' => ['length' => 8]]);

            self::assertFalse($validator->validate(['rating' => 'test']));
            self::assertTrue($validator->validate(['rating' => 5]));
            self::assertFalse($validator->validate(['rating' => 7]));
            self::assertTrue($validator->validate(['rating' => 0]));
            self::assertFalse($validator->validate(['rating' => - 5]));
            self::assertFalse($validator->validate(['vat' => '123']));
            self::assertTrue($validator->validate(['vat' => '12345678']));
            self::assertEquals(['rating' => 5], $validator->parse(['rating' => '5']));
            self::assertEquals(['vat' => 12345678], $validator->parse(['vat' => '12345678']));
        }
    }

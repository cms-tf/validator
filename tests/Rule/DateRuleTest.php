<?php
    namespace CmsTf\Validator\Tests\Rule;

    use CmsTf\Validator\Validator;
    use DateTime;
    use PHPUnit\Framework\TestCase;

    /**
     * Class DateValidationTest
     *
     * @package CmsTf\Validator\Tests
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class DateRuleTest extends TestCase {
        /**
         * Test the date validation.
         */
        public function testValidate() {
            $validator = Validator::create()->add('birthday', ['date']);

            self::assertTrue($validator->validate(['birthday' => '1999-05-01']));
            self::assertFalse($validator->validate(['birthday' => 'test']));
            self::assertFalse($validator->validate(['birthday' => '1999-13-5']));
            self::assertInstanceOf(DateTime::class, $validator->parse(['birthday' => '1999-05-01'])['birthday']);
        }
    }

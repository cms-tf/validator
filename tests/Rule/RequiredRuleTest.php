<?php
    namespace CmsTf\Validator\Tests\Rule;

    use CmsTf\Validator\Validator;
    use PHPUnit\Framework\TestCase;

    /**
     * Class RequiredValidationTest
     *
     * @package CmsTf\Validator\Tests
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class RequiredRuleTest extends TestCase {
        /**
         * Test the required validation.
         */
        public function testValidate() {
            $validator = Validator::create()->add('foo', ['required']);
            self::assertFalse($validator->validate([]));
            self::assertFalse($validator->validate(['foo' => '']));
            self::assertTrue($validator->validate(['foo' => 'bar']));
        }
    }

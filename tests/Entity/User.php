<?php

    namespace CmsTf\Validator\Tests\Entity;

    use CmsTf\Validator\Annotation\Validate;

    /**
     * Class User
     *
     * @package CmsTf\Validator\Tests\Entity
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class User {
        /**
         * @Validate({"required"})
         *
         * @var string
         */
        protected $username;

        /**
         * @Validate({"count": {"max": 3}})
         *
         * @var string[]
         */
        protected $tags;

        /**
         * @Validate({"number": {"min": 0}})
         *
         * @var int|null
         */
        protected $age;

        /**
         * @return string
         */
        public function getUsername() {
            return $this->username;
        }

        /**
         * @param string $username
         */
        public function setUsername($username) {
            $this->username = $username;
        }

        /**
         * @return string[]
         */
        public function getTags() {
            return $this->tags;
        }

        /**
         * @param string[] $tags
         */
        public function setTags($tags) {
            $this->tags = $tags;
        }

        /**
         * @return int|null
         */
        public function getAge() {
            return $this->age;
        }

        /**
         * @param int|null $age
         */
        public function setAge($age) {
            $this->age = $age;
        }
    }
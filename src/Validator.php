<?php

    namespace CmsTf\Validator;

    use CmsTf\Validator\Annotation\Validate;
    use CmsTf\Validator\Error\ErrorContainerInterface;
    use CmsTf\Validator\Error\ErrorContainerTrait;
    use CmsTf\Validator\Field\Field;
    use CmsTf\Validator\Field\FieldDefinition;
    use CmsTf\Validator\Field\FieldDefinitionInterface;
    use CmsTf\Validator\Rule\RuleManager;
    use CmsTf\Validator\Rule\RuleRegistry;
    use CmsTf\Validator\Util\ArrayUtil;
    use Doctrine\Common\Annotations\AnnotationReader;
    use ReflectionClass;
    use Symfony\Component\Translation\Loader\PhpFileLoader;
    use Symfony\Component\Translation\Translator;

    /**
     * Class Validator
     *
     * @package CmsTf\Validator
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class Validator implements ErrorContainerInterface {
        use ErrorContainerTrait;

        /**
         * @var FieldDefinitionInterface[]
         */
        protected $fieldDefinitions = [];

        /**
         * @var bool
         */
        protected $allowExtraFields = false;

        /**
         * The results.
         *
         * @var Field[]
         */
        protected $fields;

        /**
         * @var Translator
         */
        protected $translator;

        /**
         * @var RuleRegistry
         */
        protected $ruleRegistry;

        /**
         * @param string       $name
         * @param string|array $rule
         * @param array        $options
         *
         * @return $this
         */
        public function add($name, $rule = null, array $options = []) {
            if (!isset($this->fieldDefinitions[$name])) {
                $definition = new FieldDefinition();
                $definition->setValidator($this);
                $definition->setName($name);
                $this->fieldDefinitions[$name] = $definition;
            } else {
                $definition = $this->fieldDefinitions[$name];
            }

            if (is_null($rule)) {
                return $this;
            }

            if (!is_array($rule)) {
                $rule = [$rule => $options];
            }

            $rules = $definition->getRules();

            foreach ($rule as $ruleName => $ruleOptions) {
                if (is_numeric($ruleName) && is_string($ruleOptions)) {
                    $ruleName = $ruleOptions;
                    $ruleOptions = [];
                }

                if (isset($rules[$ruleName])) {
                    $rule = $rules[$ruleName];
                    $rule->setOptions(array_replace($rule->getOptions(), $ruleOptions));
                } else {
                    $rule = $this->getRuleRegistry()->get($ruleName);
                    $rule->setValidator($this);
                    $rule->setOptions($ruleOptions);
                    $rule->setFieldDefinition($definition);
                    $definition->addRule($rule);
                }
            }

            return $this;
        }

        /**
         * Get the value from the data.
         *
         * @param string|array $key The key
         * @param null         $default
         * @param bool         $parse
         *
         * @return mixed The value
         */
        public function get($key, $default = null, $parse = false) {
            $field = $this->getField($key);

            if ($field === null) {
                return $default;
            }

            $value = $field->getValue();

            if ($parse) {
                $definition = $this->getDefinition($key);

                if ($definition === null) {
                    throw new \RuntimeException('The field could not be found.');
                }

                if (!$field->isValidated()) {
                    throw new \RuntimeException('The field is not validated.');
                }

                if ($field->hasErrors()) {
                    throw new \RuntimeException('The field has errors.');
                }

                $value = $definition->parse($value);
            }

            return $value;
        }

        /**
         * Get the value from the data and parse the value.
         *
         * @param string|array $key The key
         * @param null         $default
         *
         * @return mixed The value
         */
        public function getAndParse($key, $default = null) {
            return $this->get($key, $default, true);
        }

        /**
         * @return RuleRegistry
         */
        public function getRuleRegistry() {
            return $this->ruleRegistry === null ? RuleManager::getInstance() : $this->ruleRegistry;
        }

        /**
         * @param RuleRegistry $ruleRegistry
         */
        public function setRuleRegistry($ruleRegistry) {
            $this->ruleRegistry = $ruleRegistry;
        }

        /**
         * Get the field.
         *
         * @param $fieldName
         *
         * @return FieldDefinitionInterface|null
         */
        public function getDefinition($fieldName) {
            if (isset($this->fieldDefinitions[$fieldName])) {
                return $this->fieldDefinitions[$fieldName];
            }

            foreach ($this->fieldDefinitions as $field) {
                if ($field->isName($fieldName)) {
                    return $field;
                }
            }

            return null;
        }

        /**
         * @return FieldDefinitionInterface[]
         */
        public function getDefinitions() {
            return array_values($this->fieldDefinitions);
        }

        /**
         * Get the value from the data.
         *
         * @param string|array $key The key
         *
         * @return Field|null The value
         */
        public function getField($key) {
            if (is_array($key)) {
                $array = $key;
                $key = array_shift($array);
                $count = count($array);

                for ($i = 0; $i < $count; $i ++) {
                    $key .= '[' . $array[$i] . ']';
                }
            }

            if (!isset($this->fields[$key])) {
                return null;
            }

            return $this->fields[$key];
        }

        /**
         * @return Field[]
         */
        public function getFields() {
            return $this->fields;
        }

        /**
         * @param array $data
         */
        public function setData($data) {
            $this->fields = array_map(function($value) {
                $field = new Field($value);
                $field->setTranslator($this->translator);

                return $field;
            }, ArrayUtil::flatten($data));
        }

        /**
         * Get the keys of the array.
         *
         * @param array $arr
         *
         * @return array
         */
        protected function getKeys(array $arr) {
            $return = [];
            foreach ($arr as $key => $value) {
                $return[] = $key;
                if (is_array($value)) {
                    $return = array_merge($return, $this->getKeys($value));
                }
            }

            return $return;
        }

        /**
         * Convert the data to an array.
         *
         * @param bool $parse
         *
         * @return array
         */
        public function toArray($parse = false) {
            $array = [];

            foreach ($this->fields as $key => $field) {
                if (!$field->isDefined()) {
                    continue;
                }

                $value = $field->getValue();

                $loc = &$array;
                parse_str($key, $arr);

                foreach ($this->getKeys($arr) as $step) {
                    if (!isset($loc[$step])) {
                        $loc[$step] = array();
                    }

                    $loc = &$loc[$step];
                }

                if ($field->isEmpty()) {
                    $loc = null;
                    continue;
                }

                if ($parse) {
                    $loc = $this->getDefinition($key)->parse($value);
                } else {
                    $loc = $value;
                }
            }

            return $array;
        }

        /**
         * True if the validator has errors.
         *
         * @return bool
         */
        public function hasErrors() {
            if (count($this->errors) > 0) {
                return true;
            }

            foreach ($this->fields as $field) {
                if ($field->hasErrors()) {
                    return true;
                }
            }

            return false;
        }

        /**
         * Get all the errors in the validator.
         *
         * @return string[]
         */
        public function getErrors() {
            $errors = $this->errors;

            foreach ($this->fields as $name => $field) {
                $errors = array_merge($this->errors, $field->getErrors());
            }

            return $errors;
        }

        /**
         * Get all the errors in the fields.
         *
         * @param bool $all
         *
         * @return array
         */
        public function getFieldErrors($all = true) {
            $errors = [];

            if ($all || count($this->errors) > 0) {
                $errors['_form'] = $this->errors;
            }

            foreach ($this->fields as $name => $field) {
                $fieldErrors = $field->getErrors();
                if ($all || count($fieldErrors) > 0) {
                    $errors[$name] = $fieldErrors;
                }
            }

            return $errors;
        }

        /**
         * Validate the data.
         *
         * @param array|null $data The data
         *
         * @return bool True if the data is validated.
         */
        public function validate(array $data = null) {
            if ($data !== null) {
                $this->setData($data);
            }

            $this->clearErrors();

            $foundMoreFields = false;

            foreach ($this->fields as $key => $field) {
                $definition = $this->getDefinition($key);

                if ($definition === null) {
                    $foundMoreFields = true;
                    continue;
                }

                $field->clearErrors();
                if (!$field->isEmpty()) {
                    $definition->validate($field, $field->getValue());
                }
                $field->setValidated(true);
            }

            if ($foundMoreFields && !$this->isAllowExtraFields()) {
                $this->addError('This form should not contain extra fields.');
            }

            foreach ($this->fieldDefinitions as $definition) {
                if ($definition->isMatch()) {
                    $field = null;
                } else {
                    if (isset($this->fields[$definition->getName()])) {
                        $field = $this->fields[$definition->getName()];
                    } else {
                        $field = new Field(null, false);
                        $field->setTranslator($this->translator);
                        $this->fields[$definition->getName()] = $field;
                    }
                }

                foreach ($definition->getRules() as $rule) {
                    $rule->validateForm($field);
                }
            }

            $this->setValidated(true);

            return !$this->hasErrors();
        }

        /**
         * @return bool
         */
        public function isAllowExtraFields() {
            return $this->allowExtraFields;
        }

        /**
         * @param bool $allowExtraFields
         */
        public function setAllowExtraFields($allowExtraFields) {
            $this->allowExtraFields = $allowExtraFields;
        }

        /**
         * Parse the data.
         *
         * @param array|null $data The data.
         *
         * @return array|bool False on failure, array on success.
         */
        public function parse(array $data = null) {
            if (!$this->validate($data)) {
                return false;
            }

            return $this->toArray(true);
        }

        /**
         * Add the language.
         *
         * @param $locale
         *
         * @return $this
         */
        public function setLocale($locale) {
            if ($this->translator === null) {
                $this->translator = new Translator($locale);
                $this->translator->addLoader('php', new PhpFileLoader());

                foreach (glob(__DIR__ . '/../resources/translations/*.php') as $file) {
                    $this->translator->addResource('php', $file, basename($locale, '.php'), 'validators');
                }
            } else {
                $this->translator->setLocale($locale);
            }

            return $this;
        }

        /**
         * Get the locale.
         *
         * @return null|string
         */
        public function getLocale() {
            return $this->translator === null ? null : $this->translator->getLocale();
        }

        /**
         * @return Validator
         */
        public function __clone() {
            $validator = new Validator();
            $validator->fieldDefinitions = $this->fieldDefinitions;
            $validator->setTranslator($this->translator);
            $validator->setAllowExtraFields($this->allowExtraFields);
            $validator->setRuleRegistry($this->ruleRegistry);
            return $validator;
        }

        /**
         * Create a new validator with the given definitions.
         *
         * @param object|array $definitions
         *
         * @return Validator
         */
        public function withDefinitions($definitions) {
            $validator = clone $this;
            $validator->setDefinitions($definitions);
            return $validator;
        }

        /**
         * Initialize the validator.
         *
         * @param $source
         *
         * @return $this
         */
        public function setDefinitions($source) {
            $this->fieldDefinitions = [];
            $this->fields = [];

            if (is_object($source) || is_string($source) && class_exists($source)) {
                $reader = new AnnotationReader();
                $reflectionClass = new ReflectionClass($source);

                foreach ($reflectionClass->getProperties() as $reflectionProperty) {
                    /** @var Validate $validateAnnotation */
                    $validateAnnotation = $reader->getPropertyAnnotation($reflectionProperty, Validate::class);

                    if ($validateAnnotation === null) {
                        continue;
                    }

                    $name = $validateAnnotation->name;

                    if ($name === null) {
                        $name = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $reflectionProperty->getName()));
                    }

                    // Check if the wildcard should be added.
                    if (preg_match('/@var\s+(?\'type\'[^\s]+)/', $reflectionProperty->getDocComment(), $matches)) {
                        $types = explode('|', $matches['type']);

                        foreach ($types as $type) {
                            if ($type === 'array' || substr($type, - 2) === '[]') {
                                $name .= '[*]';
                                break;
                            }
                        }
                    }

                    $this->add($name, $validateAnnotation->rules);
                }
            }

            if (is_array($source)) {
                foreach ($source as $name => $options) {
                    if (is_int($name)) {
                        $name = $options;
                        $options = array();
                    }

                    $this->add($name, $options);
                }
            }

            return $this;
        }

        /**
         * Create a new validator.
         *
         * @param null $source
         * @param null $locale
         *
         * @return Validator
         */
        public static function create($source = null, $locale = null) {
            $validator = new Validator();

            if ($locale !== null) {
                $validator->setLocale($locale);
            }

            $validator->setDefinitions($source);

            return $validator;
        }
    }
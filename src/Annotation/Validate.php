<?php

    namespace CmsTf\Validator\Annotation;

    /**
     * Class Validate
     *
     * @package CmsTf\Validator\Annotation
     * @author  Gerard Smit <gsmit1996@gmail.com>
     *
     * @Annotation
     */
    class Validate {
        /**
         * @var array
         */
        public $rules;

        /**
         * @var string|null
         */
        public $name;

        /**
         * Validate constructor.
         *
         * @param array $data
         */
        public function __construct(array $data = []) {
            $this->name = isset($data['name']) ? $data['name'] : null;
            $this->rules = isset($data['value']) ? $data['value'] : [];
        }
    }
<?php

    namespace CmsTf\Validator\Field;

    use CmsTf\Validator\Error\ErrorContainerInterface;
    use CmsTf\Validator\Rule\Rule;
    use CmsTf\Validator\Validator;

    /**
     * Interface FieldInterface
     *
     * @package CmsTf\Validator
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    interface FieldDefinitionInterface {
        /**
         * True if the field is a match.
         *
         * @return bool
         */
        public function isMatch();

        /**
         * Check if the field handles the given name.
         *
         * @param string $name The name.
         *
         * @return bool
         */
        public function isName($name);

        /**
         * Get the name of the field.
         *
         * @return string
         */
        public function getName();

        /**
         * Set the name of the field.
         *
         * @param string $name The name
         *
         * @return void
         */
        public function setName($name);

        /**
         * Get the rule.
         *
         * @param string $name The name of the rule
         *
         * @return Rule|null
         */
        public function getRule($name);

        /**
         * Get all the rules.
         *
         * @return Rule[]
         */
        public function getRules();

        /**
         * Add the rule.
         *
         * @param Rule $rule The rule.
         *
         * @return void
         */
        public function addRule(Rule $rule);

        /**
         * Get the validator.
         *
         * @return Validator
         */
        public function getValidator();

        /**
         * Set the validator.
         *
         * @param Validator $validator
         *
         * @return Validator
         */
        public function setValidator(Validator $validator);

        /**
         * Validate the data.
         *
         * If the data has an error, use ErrorInterface::addError
         *
         * @see ErrorContainerInterface::addError
         *
         * @param Field $result The result.
         * @param mixed $data   The data to validate.
         *
         * @return void
         */
        public function validate(Field $result, $data);

        /**
         * Parse the data.
         *
         * @param mixed $data The data to parse.
         *
         * @return mixed The parsed data.
         */
        public function parse($data);
    }
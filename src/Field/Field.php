<?php

    namespace CmsTf\Validator\Field;

    use CmsTf\Validator\Error\ErrorContainerInterface;
    use CmsTf\Validator\Error\ErrorContainerTrait;

    /**
     * Class Result
     *
     * @package CmsTf\Validator
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class Field implements ErrorContainerInterface {
        use ErrorContainerTrait;

        /**
         * @var mixed
         */
        protected $value;

        /**
         * @var bool
         */
        protected $defined;

        /**
         * Result constructor.
         *
         * @param mixed $value   The value.
         * @param bool  $defined True if the field was defined in the data.
         */
        public function __construct($value, $defined = true) {
            $this->value = $value;
            $this->defined = $defined;
        }

        /**
         * @param mixed $value
         */
        public function setValue($value) {
            $this->value = $value;
        }

        /**
         * @return mixed
         */
        public function getValue() {
            return $this->value;
        }

        /**
         * @return bool
         */
        public function isEmpty() {
            return !$this->isDefined() || $this->value === null || $this->value === '';
        }

        /**
         * @return bool
         */
        public function isDefined() {
            return $this->defined;
        }

        /**
         * @param bool $defined
         */
        public function setDefined($defined) {
            $this->defined = $defined;
        }
    }
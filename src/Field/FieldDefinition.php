<?php

    namespace CmsTf\Validator\Field;

    use CmsTf\Validator\Rule\Rule;
    use CmsTf\Validator\Rule\RuleManager;
    use CmsTf\Validator\Validator;

    /**
     * Class Field
     *
     * @package CmsTf\Validator
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class FieldDefinition implements FieldDefinitionInterface {
        /**
         * The name of the field.
         *
         * @var string
         */
        protected $name;

        /**
         * @var bool
         */
        protected $match;

        /**
         * @var Rule[]
         */
        protected $rules = [];

        /**
         * @var Validator
         */
        protected $validator;

        /**
         * Get the validator.
         *
         * @return Validator
         */
        public function getValidator() {
            return $this->validator;
        }

        /**
         * Set the validator.
         *
         * @param Validator $validator
         *
         * @return Validator
         */
        public function setValidator(Validator $validator) {
            $this->validator = $validator;
        }

        /**
         * {@inheritdoc}
         */
        public function validate(Field $result, $value) {
            $result->clearErrors();

            foreach ($this->rules as $rule) {
                $rule->validate($result, $value);
            }
        }

        /**
         * {@inheritdoc}
         */
        public function parse($value) {
            foreach ($this->rules as $rule) {
                $value = $rule->parse($value);
            }

            return $value;
        }

        /**
         * {@inheritdoc}
         */
        public function getRule($name) {
            return isset($this->rules[$name]) ? $this->rules[$name] : null;
        }

        /**
         * {@inheritdoc}
         */
        public function getRules() {
            return $this->rules;
        }

        /**
         * {@inheritdoc}
         */
        public function addRule(Rule $rule) {
            $name = $this->validator->getRuleRegistry()->getName($rule);

            if ($name === null) {
                $name = get_class($rule);
            }

            $this->rules[$name] = $rule;
        }

        /**
         * {@inheritdoc}
         */
        public function isMatch() {
            return $this->match;
        }

        /**
         * Check if the current name is a match.
         *
         * @return bool
         */
        protected function checkIfMatch() {
            $pos = 0;

            while ($pos = strpos($this->name, '*', $pos)) {
                if ($pos === false) {
                    return false;
                }

                if ($pos !== 0 && $this->name[$pos - 1] === '\\') {
                    $pos ++;
                    continue;
                }

                return true;
            }

            return false;
        }

        /**
         * {@inheritdoc}
         */
        public function isName($name) {
            if ($this->isMatch()) {
                return fnmatch(str_replace(['[', ']'], ['\\[', '\\]'], $this->name), $name);
            }

            return $name === $this->name;
        }

        /**
         * {@inheritdoc}
         */
        public function getName() {
            return $this->name;
        }

        /**
         * {@inheritdoc}
         */
        public function setName($name) {
            $this->name = $name;
            $this->match = $this->checkIfMatch();
        }
    }
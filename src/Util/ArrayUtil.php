<?php

    namespace CmsTf\Validator\Util;

    use RecursiveArrayIterator;
    use RecursiveIteratorIterator;

    /**
     * Class ArrayUtil
     *
     * @package CmsTf\Validator\Util
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class ArrayUtil {
        /**
         * Flatten the array.
         *
         * @param array $input The input.
         *
         * @return array
         */
        public static function flatten($input) {
            $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($input));
            $result = [];

            foreach ($iterator as $value) {
                $key = $iterator->getSubIterator(0)->key();
                $depth = $iterator->getDepth();

                for ($i = 1; $i <= $depth; $i ++) {
                    $key .= '[' . $iterator->getSubIterator($i)->key() . ']';
                }

                $result[$key] = $value;
            }

            return $result;
        }
    }
<?php

    namespace CmsTf\Validator\Error;

    use Symfony\Component\Translation\Translator;

    /**
     * Trait ErrorTrait
     *
     * @package CmsTf\Validator
     * @author  Gerard Smit <gsmit1996@gmail.com>
     *
     * @see     ErrorContainerInterface
     */
    trait ErrorContainerTrait {
        /**
         * The errors.
         *
         * @var string[]
         */
        protected $errors = [];

        /**
         * True if validated.
         *
         * @var bool
         */
        protected $validated = false;

        /**
         * @var Translator
         */
        protected $translator;

        /**
         * {@inheritdoc}
         */
        public function isValidated() {
            return $this->validated;
        }

        /**
         * {@inheritdoc}
         */
        public function setValidated($validated) {
            $this->validated = $validated;
        }

        /**
         * {@inheritdoc}
         */
        public function hasErrors() {
            return count($this->getErrors()) > 0;
        }

        /**
         * {@inheritdoc}
         */
        public function getError() {
            return $this->hasErrors() ? $this->getErrors()[0] : null;
        }

        /**
         * {@inheritdoc}
         */
        public function getErrors() {
            return $this->errors;
        }

        /**
         * @return Translator
         */
        public function getTranslator() {
            return $this->translator;
        }

        /**
         * @param Translator $translator
         *
         * @return $this
         */
        public function setTranslator($translator) {
            $this->translator = $translator;

            return $this;
        }

        /**
         * {@inheritdoc}
         */
        public function clearErrors() {
            $this->errors = [];
            $this->validated = false;
        }

        /**
         * {@inheritdoc}
         */
        public function addError($message, $parameters = array()) {
            if ($this->translator === null) {
                $string = strtr($message, $parameters);
            } else {
                $string = $this->translator->trans($message, $parameters, 'validators');
            }

            $this->errors[] = $string;
        }
    }
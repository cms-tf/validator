<?php

    namespace CmsTf\Validator\Error;

    use Symfony\Component\Translation\Translator;

    /**
     * Interface ErrorInterface
     *
     * @package CmsTf\Validator\Error
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    interface ErrorContainerInterface {
        /**
         * @return Translator
         */
        public function getTranslator();

        /**
         * @param Translator $translator
         */
        public function setTranslator($translator);

        /**
         * True if the field has errors.
         *
         * @return bool
         */
        public function hasErrors();

        /**
         * Get the first error.
         *
         * @return string|null
         */
        public function getError();

        /**
         * Get all the errors.
         *
         * @return string[]
         */
        public function getErrors();

        /**
         * Clear the errors.
         *
         * @return void
         */
        public function clearErrors();

        /**
         * Add an error on this field.
         *
         * @param string $message The message.
         * @param array  $parameters The parameters
         *
         * @return void
         */
        public function addError($message, $parameters = array());
    }
<?php

    namespace CmsTf\Validator\Rule;

    /**
     * Class CountRule
     *
     * @package CmsTf\Validator\Rule
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class CountRule extends Rule {
        /**
         * {@inheritdoc}
         */
        public function validateForm($field) {
            $count = 0;

            foreach ($this->validator->getFields() as $key => $field) {
                if ($field->isDefined() && $this->fieldDefinition->isName($key)) {
                    $count ++;
                }
            }

            if ($this->hasOption('min')) {
                $min = $this->getOption('min');

                if ($count < $min) {
                    $this->validator->addError($this->getOption('message_min', 'There should be minimal %min% values.'), ['%min%' => $min]);
                }
            }

            if ($this->hasOption('max')) {
                $max = $this->getOption('max');

                if ($count > $max) {
                    $this->validator->addError($this->getOption('message_max', 'There should be maximal %max% values.'), ['%max%' => $max]);
                }
            }
        }
    }
<?php

    namespace CmsTf\Validator\Rule;

    use CmsTf\Validator\Field\Field;
    use DateTime;

    /**
     * Class DateRule
     *
     * @package CmsTf\Validator\Rules
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class DateRule extends Rule {
        /**
         * {@inheritdoc}
         */
        public function validate(Field $field, $value) {
            $dateTime = $this->parse($value);

            if ($dateTime === false || array_sum($dateTime->getLastErrors())) {
                $field->addError($this->getOption('message', 'This field has to be a valid date.'));
            }
        }

        /**
         * {@inheritdoc}
         */
        public function parse($value) {
            return DateTime::createFromFormat($this->getOption('format', 'Y-m-d'), $value);
        }
    }
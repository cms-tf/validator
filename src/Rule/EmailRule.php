<?php

    namespace CmsTf\Validator\Rule;

    use CmsTf\Validator\Field\Field;

    /**
     * Class EmailRule
     *
     * @package CmsTf\Validator\Rules
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class EmailRule extends Rule {
        /**
         * {@inheritdoc}
         */
        public function validate(Field $field, $value) {
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $field->addError($this->getOption('message', 'This field has to be a valid e-mail.'));
            }
        }
    }
<?php

    namespace CmsTf\Validator\Rule;

    use CmsTf\Validator\Field\Field;
    use libphonenumber\NumberParseException;
    use libphonenumber\PhoneNumberFormat;
    use libphonenumber\PhoneNumberUtil;

    /**
     * Class PhoneNumberRule
     *
     * @package CmsTf\Validator\Rule
     * @author Gerard Smit <gsmit1996@gmail.com>
     */
    class PhoneNumberRule extends Rule {
        /**
         * @var array
         */
        protected $regex = array(
            'NL' => '/(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)/',
            'BE' => '/^((\+|00)32\s?|0)(\d\s?\d{3}|\d{2}\s?\d{2})(\s?\d{2}){2}$/',
            'US' => '/^\([0-9]{3}\)[0-9]{3}-[0-9]{4}$/',
            'UK' => '/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/'
        );

        /**
         * {@inheritdoc}
         */
        public function validate(Field $field, $value) {
            if ($this->parse($value) === null) {
                $field->addError($this->getOption('message', 'This field has to be a valid phone number.'));
            }
        }

        /**
         * @inheritDoc
         */
        public function parse($value) {
            $region = $this->getOption('region', 'NL');

            if (class_exists(PhoneNumberUtil::class)) {
                $phoneUtil = PhoneNumberUtil::getInstance();

                try {
                    $phoneNumber = $phoneUtil->parse($value, $region);

                    if (!$phoneUtil->isValidNumber($phoneNumber)) {
                        return null;
                    }

                    return $phoneUtil->format($phoneNumber, $this->getOption('format', PhoneNumberFormat::NATIONAL));
                } catch (NumberParseException $exception) {
                    return null;
                }
            }

            if (isset($this->regex[$region])) {
                $regex = $this->regex[$region];
            } else {
                throw new \RuntimeException('The region ' . $region .
                    ' is not supported. Install libphonenumber for full support.');
            }

            return preg_match($regex, $value) ? $value : null;
        }
    }
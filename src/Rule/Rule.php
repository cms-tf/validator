<?php

    namespace CmsTf\Validator\Rule;

    use CmsTf\Validator\Field\Field;
    use CmsTf\Validator\Field\FieldDefinitionInterface;
    use CmsTf\Validator\Validator;

    /**
     * Interface Rule
     *
     * @package CmsTf\Validator\Rules
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    abstract class Rule {
        /**
         * @var FieldDefinitionInterface
         */
        protected $fieldDefinition;

        /**
         * @var Validator
         */
        protected $validator;

        /**
         * @var array
         */
        protected $options;

        /**
         * @return Validator
         */
        public function getValidator() {
            return $this->validator;
        }

        /**
         * @param Validator $validator
         */
        public function setValidator($validator) {
            $this->validator = $validator;
        }

        /**
         * @return FieldDefinitionInterface
         */
        public function getFieldDefinition() {
            return $this->fieldDefinition;
        }

        /**
         * @param FieldDefinitionInterface $fieldDefinition
         */
        public function setFieldDefinition($fieldDefinition) {
            $this->fieldDefinition = $fieldDefinition;
        }

        /**
         * @return array
         */
        public function getOptions() {
            return $this->options;
        }

        /**
         * Set the option.
         *
         * @param string $key
         * @param mixed $value
         */
        public function setOption($key, $value) {
            $this->options[$key] = $value;
        }

        /**
         * @param array $options
         */
        public function setOptions($options) {
            $this->options = $options;
        }

        /**
         * @param $key
         *
         * @return bool
         */
        public function hasOption($key) {
            return isset($this->options[$key]);
        }

        /**
         * @param string $key
         * @param mixed  $default
         *
         * @return mixed
         */
        public function getOption($key, $default = null) {
            return $this->hasOption($key) ? $this->options[$key] : $default;
        }

        /**
         * Validate the field.
         *
         * @param Field|null $field
         *
         * @return void
         */
        public function validateForm($field) {

        }

        /**
         * Validate the field.
         *
         * @param Field $field
         * @param mixed $value
         */
        public function validate(Field $field, $value) {

        }

        /**
         * Parse the value.
         *
         * @param $value
         *
         * @return mixed
         */
        public function parse($value) {
            return $value;
        }
    }
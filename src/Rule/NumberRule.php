<?php

    namespace CmsTf\Validator\Rule;

    use CmsTf\Validator\Field\Field;

    /**
     * Class NumberRule
     *
     * @package CmsTf\Validator\Rules
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class NumberRule extends Rule {
        /**
         * {@inheritdoc}
         */
        public function validate(Field $field, $value) {
            if (!is_numeric($value)) {
                $field->addError($this->getOption('message', 'This field has to be numeric.'));

                return;
            }

            if ($this->hasOption('length')) {
                $length = $this->getOption('length');

                if (strlen($value) !== $length) {
                    $field->addError($this->getOption('message_length', 'The value length must be %length% numbers.'), ['%length%' => $length]);
                }
            }

            if ($this->hasOption('min')) {
                $min = $this->getOption('min');

                if ($value < $min) {
                    $field->addError($this->getOption('message_min', 'The minimal value is %min%.'), ['%min%' => $min]);
                }
            }

            if ($this->hasOption('max')) {
                $max = $this->getOption('max');

                if ($value > $max) {
                    $field->addError($this->getOption('message_max', 'The maximal value is %max%.'), ['%max%' => $max]);
                }
            }
        }

        /**
         * {@inheritdoc}
         */
        public function parse($value) {
            return intval($value);
        }
    }
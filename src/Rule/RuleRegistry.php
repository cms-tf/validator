<?php

    namespace CmsTf\Validator\Rule;

    /**
     * Class RuleRegistry
     *
     * @package CmsTf\Validator\Rule
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class RuleRegistry {
        /**
         * The registered rules.
         *
         * @var array
         */
        protected $rules = [
            'number' => ['class' => NumberRule::class],
            'date' => ['class' => DateRule::class],
            'email' => ['class' => EmailRule::class],
            'required' => ['class' => RequiredRule::class],
            'count' => ['class' => CountRule::class],
            'phone_number' => ['class' => PhoneNumberRule::class]
        ];

        /**
         * Register a new type.
         *
         * @param string $name  The name.
         * @param string $class The class.
         */
        public function register($name, $class) {
            $arguments = func_get_args();
            $name = array_shift($arguments);
            $class = array_shift($arguments);
            $this->rules[$name] = compact($class, $arguments);
        }

        /**
         * Register a new type.
         *
         * @param string $name  The name.
         * @param string $class The class.
         * @param array  $arguments The arguments.
         */
        public function registerArgs($name, $class, $arguments = array()) {
            $this->rules[$name] = compact($class, $arguments);
        }

        /**
         * Register the default rules.
         */
        public function clear() {
            $this->rules = [];
        }

        /**
         * Get the class name.
         *
         * @param object|string $input
         *
         * @return null|string
         */
        public function getName($input) {
            if (is_object($input)) {
                $input = get_class($input);
            }

            foreach ($this->rules as $name => $className) {
                if ($className['class'] === $input) {
                    return $name;
                }
            }

            return null;
        }

        /**
         * @param $name
         *
         * @return Rule
         */
        public function get($name) {
            $information = $this->rules[$name];
            $reflection = new \ReflectionClass($information['class']);

            /** @var Rule $instance */
            $instance = $reflection->newInstanceArgs(isset($information['arguments']) ? $information['arguments'] : array());

            return $instance;
        }
    }
<?php

    namespace CmsTf\Validator\Rule;

    /**
     * Class RequiredRule
     *
     * @package CmsTf\Validator\Rule
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class RequiredRule extends Rule {
        /**
         * {@inheritdoc}
         */
        public function validateForm($field) {
            $addedMessage = false;

            foreach ($this->validator->getFields() as $key => $formField) {
                if ($this->fieldDefinition->isName($key)) {
                    $addedMessage = true;

                    if ($formField->isEmpty()) {
                        $formField->addError($this->getOption('message', 'This field is required.'));
                    }
                }
            }

            if (!$addedMessage) {
                $this->validator->addError($this->getOption('form_message', 'The form is incomplete.'));
            }
        }
    }
<?php

    namespace CmsTf\Validator\Rule;

    /**
     * Class RuleManager
     *
     * @package CmsTf\Validator
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class RuleManager {
        /**
         * @var RuleRegistry
         */
        protected static $ruleRegistry;

        /**
         * Get the registry instance.
         *
         * @return RuleRegistry
         */
        public static function getInstance() {
            if (self::$ruleRegistry === null) {
                self::$ruleRegistry = new RuleRegistry();
            }

            return self::$ruleRegistry;
        }

        /**
         * Register a new type.
         *
         * @param string $name  The name.
         * @param string $class The class.
         */
        public static function register($name, $class) {
            self::getInstance()->register($name, $class);
        }

        /**
         * Get the class name.
         *
         * @param object|string $input
         *
         * @return null|string
         */
        public static function getName($input) {
            return self::getInstance()->getName($input);
        }

        /**
         * @param $name
         *
         * @return Rule
         */
        public static function get($name) {
            return self::getInstance()->get($name);
        }
    }
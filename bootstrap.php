<?php
    use Doctrine\Common\Annotations\AnnotationRegistry;

    AnnotationRegistry::registerFile(__DIR__ . '/src/Annotation/Validate.php');
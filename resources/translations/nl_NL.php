<?php
    return array(
        // Validator
        'This form should not contain extra fields.' => 'Dit formulier mag geen extra velden bevatten.',

        // CountValidation
        'There should be minimal %min% values.' => 'Er moeten minimaal %min% waardes zijn.',
        'There should be maximal %max% values.' => 'Er moeten maximaal %min% waardes zijn.',

        // DateValidation
        'This field has to be a valid date.' => 'Dit veld moet een geldige datum zijn.',

        // EmailValidation
        'This field has to be a valid e-mail.' => 'Dit veld moet een geldig e-mailadres zijn.',

        // PhoneNumber
        'This field has to be a valid phone number.' => 'Dit veld moet een geldig telefoonnummer zijn.',

        // NotEmptyValidation
        'The field should not be empty.' => 'Dit veld mag niet leeg zijn.',

        // NumberValidation
        'This field has to be numeric.' => 'Dit veld zou numeriek zijn.',
        'The minimal value is %min%.' => 'De minimale waarde is %min%.',
        'The maximal value is %max%.' => 'De maximale waarde is %max%.',
        'The length must be %length% numbers.' => 'De waarde moet %length% cijfers hebben.',

        // RequiredValidation
        'This field is required.' => 'Dit veld is verplicht.',
        'The form is incomplete.' => 'Het formulier is incompleet.'
    );